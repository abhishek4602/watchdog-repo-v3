import React, { Component,useState } from 'react';  
import { createStackNavigator } from '@react-navigation/stack';
import {Picker} from '@react-native-picker/picker';
import { NavigationContainer } from '@react-navigation/native';
import { Platform, StyleSheet, View, Text,  

Image, TouchableOpacity, Alert, CheckBox} from 'react-native';  
import { TouchableHighlight } from 'react-native-gesture-handler';
import 'react-native-gesture-handler';
 
 function Logs () {
    const onPress  = () =>{}
    const [isSelected, setSelection] = useState(false);
     
    let [ showfilter, setShowFilter ] = React.useState(false);
    let [ Awing, setAWing ] = React.useState(true);   let [ Bwing, setBWing ] = React.useState(true);
    
    let [ isPress, setIsPress ] = React.useState(false);
    let [ isTimeAscendingPress, setIsTimeAscendingPress ] = React.useState(true);
    let [ isTimeDescendingPress, setIsTimeDescendingPress ] = React.useState(false);
    let [ isDeliveryPress, setIsDeliveryPress ] = React.useState(false);
    let [ isServicesPress, setIsServicesPress ] = React.useState(false);
    let [ isGuestPress, setIsGuestPress ] = React.useState(false);
    let [ isTodayPress, setIsTodayPress ] = React.useState(false);
    let [ isYesterdayPress, setIsYesterdayPress ] = React.useState(false);

    
    let touchProps = {
        activeOpacity: 1,
        underlayColor: '#FFFFFF',                               // <-- "backgroundColor" will be always overwritten by "underlayColor"
        style: isPress ? styles.pressed : styles.nonPressed, // <-- but you can still apply other style changes
   //     onHideUnderlay: () => setIsPress(false),
     //   onShowUnderlay: () => setIsPress(false),
        onPress: () => setIsPress(!isPress),                 // <-- "onPress" is apparently required
      };

    let touchPropsText = {

        style : isPress ? styles.textPressed:styles.textNonPressed
    } 

  const [flatVisited, setflatVisited] = useState("");
    return(
        <View>
        <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:20,fontSize:15,marginTop:10,marginBottom:10}}>Visitor Logs</Text>  
        <View   style={{ flexDirection: 'row',width:'60%'}}>
             
  
             <TouchableOpacity
               onPress={()=>setShowFilter(!showfilter)}
             style={{borderColor:'#1C5FC1',backgroundColor:'#1C5FC1',borderWidth:0,marginStart:30,height:23,width:100,borderRadius:15}}
             ><View style={{flexDirection:"row"}}>

            <Image 
               source={require('../assets/filter.png')} alignItems='center'
              style={{width:10,height:10,marginTop:7 ,marginStart:12 ,marginEnd:5}}
              />
             <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:12,marginTop:2,marginBottom:0,color:'#FFFFFF'}}>FILTER</Text>  
             <Image 
                                source={require('../assets/right-arrow.png')} alignItems='center'
                                style={{width:10,height:8,marginTop:7 ,marginStart:8 }}
                    />
             </View>
                     </TouchableOpacity>
                <View>
                    
                </View>
              
        </View>
        {showfilter? (
        <View style={{backgroundColor :'#F1F6FF', margin:10,borderRadius:5}}>
           
        <View style={{marginTop:10}}>
             <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:35,fontSize:10,marginTop:0,marginBottom:10,color:'#1c2833'}}>Visitor Type</Text>  
             </View>

        <View style={{ flexDirection: 'row'}}>


            <View style={{flex:1,marginStart:40}}>

            <TouchableOpacity 
              //underlayColor='#FFFFFF' 
               style ={isGuestPress? styles.pressed :styles.nonPressed}
                onPress ={()=>setIsGuestPress (!isGuestPress)}
              // onPress ={()=>console.log(isGuestPress)}
        >
                        <Text     style ={isGuestPress? styles.textPressed :styles.textNonPressed}  >GUEST</Text> 

            </TouchableOpacity>
          
            </View>
        
            <View style={{flex:1}}>
            <TouchableOpacity  
            
            style ={isDeliveryPress? styles.pressed :styles.nonPressed}
            underlayColor='#FFFFFF'
            onPress ={()=>setIsDeliveryPress (!isDeliveryPress)}
                >
                     <Text     style ={isDeliveryPress? styles.textPressed :styles.textNonPressed}  >DELIVERY</Text> 

         </TouchableOpacity>
         </View>
         <View style={{flex:1,marginEnd:80}}>
            <TouchableOpacity  
            
            style ={isServicesPress? styles.pressed :styles.nonPressed}
            underlayColor='#FFFFFF'
            onPress ={()=>setIsServicesPress (!isServicesPress)}
                >
                     <Text     style ={isServicesPress? styles.textPressed :styles.textNonPressed}  >SERVICES</Text> 

         </TouchableOpacity>
         </View>
         
     
            
        </View>
     
      
         <View>
             <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:35,fontSize:10,marginTop:0,marginBottom:10,color:'#1c2833'}}>Date</Text>  
             </View>

        <View style={{ flexDirection: 'row'}}>


<View style={{flex:1,marginStart:40,width:'80%'}}>

<TouchableOpacity 
  underlayColor='#FFFFFF' 
   style ={isTodayPress? styles.pressed :styles.nonPressed}
   onPress ={()=>setIsTodayPress (!isTodayPress)}
>
            <Text     style ={isTodayPress? styles.textPressed :styles.textNonPressed}  >TODAY</Text> 

</TouchableOpacity>

</View>

 
<View style={{flex:1,marginEnd:150}}>
<TouchableOpacity  

style ={isYesterdayPress? styles.pressed :styles.nonPressed}
underlayColor='#FFFFFF'
onPress ={()=>setIsYesterdayPress (!isYesterdayPress)}
    >
         <Text     style ={isYesterdayPress? styles.textPressed :styles.textNonPressed}  >YESTERDAY</Text> 

</TouchableOpacity>
</View>



</View>

<View>
             <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:35,fontSize:10,marginTop:0,marginBottom:10,color:'#1c2833'}}>Flat Number</Text>  
             </View>
             <View style={{flexDirection:"row"}}>
             <View style={{ flexDirection: 'row'}}>
       <View style={{marginStart:40,marginEnd:10}}>
       <TouchableOpacity  

                style ={Awing? styles.pressedSort :styles.nonPressedSort}
                underlayColor='#FFFFFF'
                onPress ={()=>{
                    setAWing (true)
             setBWing(false)
                }}
    >
         <Text     style ={Awing? styles.textPressedSort :styles.textNonPressedSort}  >A WING</Text> 

</TouchableOpacity>

            </View>
            <View>
            <TouchableOpacity  

style ={Bwing? styles.pressedSort :styles.nonPressedSort}
underlayColor='#FFFFFF'
onPress ={()=>{
    setBWing (true)
  setAWing(false)
}}
>
<Text     style ={Bwing? styles.textPressedSort :styles.textNonPressedSort}  >B WING</Text> 

</TouchableOpacity>

            </View>
       </View>

                 
             </View>
                 <Picker
                    itemTextStyle={{   color: "blue", fontFamily:"MS", fontSize:8 }}
  
                    style={{marginStart:120,height: 20, width: 100,marginTop:20}}
                >
   <Picker.Item fontFamily='MS' fontSize='8' label="101" value="101" />
   <Picker.Item  fontFamily='MS' fontSize='8' label="102" value="102" />
</Picker> 
          
             <View style={{marginStart:250,marginTop:15,marginBottom:10}} >    
             
             <TouchableOpacity             
                  style ={  {backgroundColor:'FFFFFF',borderRadius:20,borderWidth:0,backgroundColor:'#1C5FC1',width:85,height:30 } }
                  underlayColor='#FFFFFF'
         
                >
                      <View style={{ flexDirection: 'row'}}>
                      <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:15,fontSize:12,marginTop:5,marginBottom:15,color:'#FFFFFF'}}>APPLY</Text>  
                             <Image 
                                source={require('../assets/checked.png')} alignItems='center'
                                style={{width:10,height:10,marginTop:9 ,marginStart:6 }}>
                             </Image>
                          
                      </View>   
                          
                  
  
              </TouchableOpacity>
              
              </View>
        
             
             </View>
            ) : <View style={{height:20}}></View> }
        <View style={{ flexDirection: 'row'}}>
        <TouchableOpacity
               
             style={{backgroundColor:'#1C5FC1',borderWidth:0,marginBottom:10,marginStart:30,height:23,width:90,borderRadius:15}}
             ><View style={{flexDirection:"row"}}>

            <Image 
               source={require('../assets/sort.png')} alignItems='center'
              style={{width:10,height:10,marginTop:6 ,marginStart:12 ,marginEnd:5}}
              />
             <Text style={{textAlign: 'left',fontFamily:'MS',marginStart:5,fontSize:12,marginTop:2,marginBottom:0,color:'#FFFFFF'}}>SORT</Text>  
             <Image 
                                source={require('../assets/right-arrow.png')} alignItems='center'
                                style={{width:10,height:8,marginTop:7 ,marginStart:8 }}
                    />
             </View>
                     </TouchableOpacity>


        </View>
        <View style={{ flexDirection: 'row'}}>
       <View style={{marginStart:65,marginEnd:10}}>
       <TouchableOpacity  

                style ={isTimeAscendingPress? styles.pressedSort :styles.nonPressedSort}
                underlayColor='#FFFFFF'
                onPress ={()=>{
                    setIsTimeAscendingPress (true)
             setIsTimeDescendingPress(false)
                }}
    >
         <Text     style ={isTimeAscendingPress? styles.textPressedSort :styles.textNonPressedSort}  >TIME:ASCENDING</Text> 

</TouchableOpacity>

            </View>
            <View>
            <TouchableOpacity  

style ={isTimeDescendingPress? styles.pressedSort :styles.nonPressedSort}
underlayColor='#FFFFFF'
onPress ={()=>{
    setIsTimeDescendingPress (true)
  setIsTimeAscendingPress(false)
}}
>
<Text     style ={isTimeDescendingPress? styles.textPressedSort :styles.textNonPressedSort}  >TIME:DESCENDING</Text> 

</TouchableOpacity>

            </View>
       </View>

        </View>
        
    );
}

const styles = StyleSheet.create({
  cardHeaderText:
  {
   fontFamily:'MS',
    margin:10,
    fontSize:30,    
   alignItems:'flex-start',
    color:"#D5D8DC",
    textAlign:'left'
  }, 
  nonPressed:
  {
    borderColor:'#1C5FC1',
  
    width:75,
    borderWidth:1,
    borderRadius:12,
    height:20,
    marginStart:0,
    marginBottom:12
},

    textNonPressed :{
        textAlign: "center",
        fontFamily:'MS',
     //   marginStart:5,
        fontSize:10,
        marginTop:2,
        marginBottom:10,
        color:'#1C5FC1'
    },
    
    textPressed:{
        textAlign: 'center',
        fontFamily:'MS',
       // marginStart:5,
        fontSize:10,
        marginTop:2,
        marginBottom:10,
        color:'#FFFFFF'
    },
  pressed:
  {
  backgroundColor:'#1C5FC1',
  width:75,
  //borderWidth:1,
  borderRadius:12,
  height:20,
  marginStart:0,
  marginBottom:12},
  nonPressedSort:
  {
    borderColor:'#1C5FC1',
  
    width:115,
    borderWidth:1,
 
    height:20,
    marginStart:0,
    marginBottom:12
},

    textNonPressedSort :{
        textAlign: "center",
        fontFamily:'MS',
     //   marginStart:5,
        fontSize:10,
        marginTop:2,
        marginBottom:10,
        color:'#1C5FC1'
    },
    
    textPressedSort:{
        textAlign: 'center',
        fontFamily:'MS',
       // marginStart:5,
        fontSize:10,
        marginTop:2,
        marginBottom:10,
        color:'#FFFFFF'
    },
  pressedSort:
  {
  backgroundColor:'#1C5FC1',
  width:115,
 // borderWidth:1,
  
  height:20,
  marginStart:0,
  marginBottom:12}
});

export default Logs;