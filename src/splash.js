
 
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    Text,
    StatusBar,
  } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import moment from 'moment';
import React,{useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

function SplashScreen({navigation}){
    navigation.setOptions({headerShown: false});
    setTimeout(function(){
  
       
      {navigation.navigate('Home')}
  
    }, 5000);
  return(
  <View style={styles.splashContainer}>
  <Image 
            source={require('../assets/dog-1.png')} alignItems='center'
           style={{width:350,height:350,marginTop:10 ,marginStart:-20 }}
          />
  </View>
  );
  
  }
   
  const styles = StyleSheet.create({
  splashContainer: {
    flex: 1,
   // marginTop:45,
      backgroundColor: '#FFFFFF',
    
    alignItems: 'center',
    justifyContent: 'flex-start',
   // height:'50%'
  },
});
 

export default SplashScreen;