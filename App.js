import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import moment from 'moment';
import React,{useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from  './src/splash'
import HomeScreen from './src/home'
import DetailsScreen from './src/log'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/FontAwesome';

function App() {  
  return (
    <NavigationContainer> 
    <Tab.Navigator
     initialRouteName="Splash"
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'NEW VISITOR') {
              iconName = focused
                ? 'plus-circle'
                : 'plus';
            } else if (route.name === 'VISITOR LOGS') {
              iconName = focused ? 'server' : 'server';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          
          activeTintColor: '#1C2833',
          
          inactiveTintColor: '#ABB2B9',
          labelStyle:{
            fontFamily:'MS',
            marginBottom:10
          }
        }}
      >
    
      {/* <Tab.Screen name="Splash" component={SplashScreen} /> */}
        <Tab.Screen name="NEW VISITOR" component={HomeScreen} />
        <Tab.Screen name="VISITOR LOGS" component={DetailsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
export default App;
